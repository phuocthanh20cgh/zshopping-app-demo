# ZShopping 

## ZMP CLI Options

ZMP app created with following options:

```
{
  "cwd": "C:\\Users\\Admin\\ZShopping",
  "newProject": true,
  "name": "ZShopping ",
  "package": "zmp-ui",
  "framework": "react-typescript",
  "cssPreProcessor": "scss",
  "template": "blank",
  "includeTailwind": true,
  "theming": {
    "color": "#007aff",
    "darkTheme": false,
    "fillBars": false
  },
  "customBuild": false,
  "stateManagement": "recoil"
}
```

## NPM Scripts

* 🔥 `start` - run development server
* 🙏 `deploy` - deploy mini app for production