
export interface PercentSale {
    type: "percent";
    percent: number;
}

export interface FixedSale {
    amount: number;
    type: "fixed";
}

export type Sale = PercentSale | FixedSale;

export interface BaseVariant {
    id: string,
    label?: string,
    priceChange?: SVGAElement;
}

export interface SingleOptionVariant extends BaseVariant {
    type: "single";
    default?: string
}

export interface MultipleOptionVariant extends BaseVariant {
    type: "multiple";
    defaut?: string[];
}

export type Variant = SingleOptionVariant | MultipleOptionVariant;

export interface Product {
    id: number;
    name: string;
    image: string;
    price: number;
    categoryId: string[];
    description?: string;
    sale?: Sale;
    variants?: Variant[];
}