import { all, fork } from "redux-saga/effects";
import authSaga from "./infoUser/saga";
import { watchCreateDummyStore } from "./store/saga";

export default function* rootSaga() {
  yield all([fork(authSaga)]);
  yield all([fork(watchCreateDummyStore)]);
}
