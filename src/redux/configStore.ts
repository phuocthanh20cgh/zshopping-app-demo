import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { persistStore } from "redux-persist";
import { applyMiddleware, Store } from "redux";
import rootReducer from "./rootReducer";
import rootSaga from "./rootSaga";
import { configureStore, EnhancedStore } from "@reduxjs/toolkit";

let middlewares: any[] = [];

const sagaMiddleware = createSagaMiddleware();

middlewares = [...middlewares, logger, sagaMiddleware];

export const middleware = applyMiddleware(...middlewares);

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(middlewares),
});

export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
