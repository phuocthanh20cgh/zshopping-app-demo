import { combineReducers } from "redux";
import { PersistConfig, persistReducer } from "redux-persist";
import InfoUserReducer from "./infoUser/reducer";
import localStorage from "redux-persist/es/storage";
import StoreReducer from "./store/reducer";

const authPersistConfig = {
  key: "infoUser",
  storage: localStorage,
};
const storePersistConfig = {
  key: "store",
  storage: localStorage,
};

const rootReducer = combineReducers({
  infoUser: persistReducer(authPersistConfig, InfoUserReducer),
  store: persistReducer(storePersistConfig, StoreReducer),
});

// export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
