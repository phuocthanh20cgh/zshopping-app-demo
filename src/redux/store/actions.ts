import {
  GET_STORE_FAILURE,
  GET_STORE_REQUEST,
  GET_STORE_SUCCESS,
} from "./actionTypes";
import {
  SuccessPayload,
  GetStoreFailureAction,
  GetStoreSuccessAction,
} from "./types";
// Adjust the action creator to expect the correct payload structure
export const getStoreSuccess = (
  payload: SuccessPayload
): GetStoreSuccessAction => ({
  type: GET_STORE_SUCCESS,
  payload: payload,
});
// export const getStoreRequest = (
//   onSuccess?: () => void,
//   onFailed?: () => void
// ): GetStoreRequestAction => ({
//   type: GET_STORE_REQUEST,
//   onSuccess: onSuccess,
//   onFailed: onFailed,
// });

export const getStoreFailed = (error: string): GetStoreFailureAction => ({
  type: GET_STORE_FAILURE,
  error,
});

// Trong file actions.ts
export const getStoreRequest = () => ({
  type: GET_STORE_REQUEST,
});
