import { RootState } from "../configStore";

export const selectStore = (state: RootState) => state.store;
