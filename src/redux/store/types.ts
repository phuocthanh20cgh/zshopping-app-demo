import { Store } from "../../models";
import { GET_STORE_SUCCESS } from "./actionTypes";
import { GET_STORE_FAILURE, GET_STORE_REQUEST } from "../store/actionTypes";

export interface SuccessPayload {
  store: Store;
}
export interface StoreState {
  store: Store;
}

export interface GetStoreRequestAction {
  type: typeof GET_STORE_REQUEST;
  onSuccess?: () => void;
  onFailed?: () => void;
}

export interface GetStoreSuccessAction {
  type: typeof GET_STORE_SUCCESS;
  payload: SuccessPayload;
}

export interface GetStoreFailureAction {
  type: typeof GET_STORE_FAILURE;
  error: string;
}

export type StoreActionTypes =
  | GetStoreRequestAction
  | GetStoreSuccessAction
  | GetStoreFailureAction;
