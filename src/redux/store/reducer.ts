import { Store } from "../../models";
import {
  GET_STORE_FAILURE,
  GET_STORE_REQUEST,
  GET_STORE_SUCCESS,
} from "./actionTypes";

import { StoreActionTypes, StoreState } from "./types";

const initialState: StoreState = {
  store: {
    address: "",
    bannerStore: "",
    categories: [],
    followers: 0,
    id: 0,
    listProducts: [],
    logoStore: "",
    nameStore: "",
    type: "business",
  },
};

const StoreReducer = (state = initialState, action: StoreActionTypes) => {
  switch (action.type) {
    case GET_STORE_REQUEST:
      return { ...state };
    case GET_STORE_SUCCESS:
      return {
        ...state,
        store: action.payload.store,
      };
    case GET_STORE_FAILURE:
      return { ...state };

    default:
      return state;
  }
};

export default StoreReducer;
