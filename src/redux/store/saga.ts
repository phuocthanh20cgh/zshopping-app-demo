import { put, takeLatest } from "redux-saga/effects";
import { getStoreSuccess } from "./actions";
import {
  GET_STORE_FAILURE,
  GET_STORE_REQUEST,
  GET_STORE_SUCCESS,
} from "./actionTypes";
import { createDummyStore } from "../../dummy/utils";

// Worker Saga để tạo dữ liệu cửa hàng giả
function* getStoreHandle() {
  try {
    const dummyStore = createDummyStore();

    console.log(dummyStore);

    // Dispatch action thành công nếu không có lỗi
    // yield put({ type: GET_STORE_SUCCESS, payload: dummyStore });
    yield put(getStoreSuccess({ store: dummyStore }));
  } catch (error) {
    // Dispatch action thất bại nếu có lỗi xảy ra
    yield put({ type: GET_STORE_FAILURE, payload: error });
  }
}

// Watcher Saga để theo dõi action gọi việc tạo dữ liệu cửa hàng giả
export function* watchCreateDummyStore() {
  yield takeLatest(GET_STORE_REQUEST, getStoreHandle);
}
