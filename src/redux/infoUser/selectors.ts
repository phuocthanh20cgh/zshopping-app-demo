import { RootState } from "../configStore";

export const selectInfoUser = (state: RootState) => state.infoUser;
