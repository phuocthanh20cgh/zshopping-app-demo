import {
  GET_INFO_USER_FAILURE,
  GET_INFO_USER_REQUEST,
  GET_INFO_USER_SUCCESS,
} from "./actionTypes";

export interface InfoUser {
  name?: string;
  avatar?: string;
}

export interface SuccessPayload {
  name?: string;
  avatar?: string;
}

export interface GetInfoUserRequestAction {
  type: typeof GET_INFO_USER_REQUEST;
  payload: InfoUser;
  onSuccess?: () => void;
  onFailed?: () => void;
}

export interface GetInfoUserSuccessAction {
  type: typeof GET_INFO_USER_SUCCESS;
  payload: SuccessPayload;
}

export interface GetInfoUserFailureAction {
  type: typeof GET_INFO_USER_FAILURE;
  error: string;
}

export type InfoUserActionTypes =
  | GetInfoUserRequestAction
  | GetInfoUserSuccessAction
  | GetInfoUserFailureAction;
