import { GET_STORE_REQUEST } from "../store/actionTypes";
import {
  GET_INFO_USER_FAILURE,
  GET_INFO_USER_REQUEST,
  GET_INFO_USER_SUCCESS,
} from "./actionTypes";
import {
  SuccessPayload,
  InfoUser,
  GetInfoUserFailureAction,
  GetInfoUserSuccessAction,
  GetInfoUserRequestAction,
} from "./types";

export const getInfoUserRequest = (
  payload: InfoUser,
  onSuccess?: () => void,
  onFailed?: () => void
): GetInfoUserRequestAction => ({
  type: GET_INFO_USER_REQUEST,
  payload: payload,
  onSuccess: onSuccess,
  onFailed: onFailed,
});

export const getInfoUserSuccess = (
  payload: SuccessPayload
): GetInfoUserSuccessAction => ({
  type: GET_INFO_USER_SUCCESS,
  payload: payload,
});

export const getInfoUserFailed = (error: string): GetInfoUserFailureAction => ({
  type: GET_INFO_USER_FAILURE,
  error,
});
export const getStoreRequest = () => ({
  type: GET_STORE_REQUEST,
});
