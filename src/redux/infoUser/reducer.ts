import {
  GET_INFO_USER_FAILURE,
  GET_INFO_USER_REQUEST,
  GET_INFO_USER_SUCCESS,
} from "./actionTypes";

import { InfoUserActionTypes, InfoUser } from "./types";

const initialState: InfoUser = {
  name: "",
  avatar: "",
};

const InfoUserReducer = (state = initialState, action: InfoUserActionTypes) => {
  switch (action.type) {
    case GET_INFO_USER_REQUEST:
      return { ...state };
    case GET_INFO_USER_SUCCESS:
      return {
        ...state,
        name: action.payload.name,
        avatar: action.payload?.avatar,
      };
    case GET_INFO_USER_FAILURE:
      return { ...state };

    default:
      return state;
  }
};

export default InfoUserReducer;
