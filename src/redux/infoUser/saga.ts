import { put, takeLatest } from "redux-saga/effects";
import { getInfoUserFailed, getInfoUserSuccess } from "./actions";
import { GET_INFO_USER_REQUEST, GET_INFO_USER_SUCCESS } from "./actionTypes";

function* getInfoUserHandle(action: any) {
  const { onSuccess, payload, onFailed } = action;

  try {
    // goi api
    // const data = yield call(getInfoUser, payload);

    yield put(
      getInfoUserSuccess({
        avatar: payload?.avatar,

        name: payload?.name,
      })
    );

    onSuccess?.(action);
  } catch (error: any) {
    yield put(getInfoUserFailed(error));
    onFailed?.(action);
  }
}

function* authSaga() {
  yield takeLatest(GET_INFO_USER_REQUEST, getInfoUserHandle);
}

export default authSaga;
