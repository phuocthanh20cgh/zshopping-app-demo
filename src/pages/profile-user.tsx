import React, { useEffect } from "react";
import { Avatar, Box, Button, Page, Text } from "zmp-ui";
import { getInfoUserRequest } from "../redux/infoUser/actions";
import { selectInfoUser } from "../redux/infoUser/selectors";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import {
  getUserInfo,
  getAccessToken,
  requestSendNotification,
} from "zmp-sdk/apis";
import axios from "axios";

const Profile: React.FunctionComponent = () => {
  const dispatch = useAppDispatch();

  const { name, avatar } = useAppSelector(selectInfoUser);

  const handleTestRedux = () => {
    dispatch(
      getInfoUserRequest({
        avatar:
          "https://images2.thanhnien.vn/zoom/686_429/528068263637045248/2023/3/4/01-16779105613331036359642-0-0-562-900-crop-16779107315421275464572.jpeg",
        name: "Lee Min Ho",
      })
    );
  };

  const appPermission = async () => {
    try {
      const accessToken = await getAccessToken();

      // Gọi API để lấy thông tin người dùng
      await axios.get("https://graph.zalo.me/v2.0/me?fields=id,name,picture", {
        headers: {
          access_token: accessToken,
        },
      });

      const response = await getUserInfo();

      dispatch(
        getInfoUserRequest({
          avatar: response.userInfo.avatar,
          name: response.userInfo.name,
        })
      );
    } catch (error) {
      console.log("Error", error);
    } finally {
      await requestSendNotification();
    }
  };

  useEffect(() => {
    appPermission();
  }, []);

  return (
    <Page className="page">
      <Box
        style={{
          borderColor: "black",
        }}
        alignItems="center"
        flexDirection="column"
      >
        <Avatar
          story="seen"
          size={96}
          online
          src={avatar?.toString().startsWith("http") ? avatar : undefined}
        >
          {avatar}
        </Avatar>
        <Text>{name}</Text>
      </Box>

      <Button style={{ marginTop: 50 }} onClick={handleTestRedux}>
        Clear Info
      </Button>
    </Page>
  );
};

export default Profile;