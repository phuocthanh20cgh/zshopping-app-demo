import React, { Suspense, useCallback, useEffect, useMemo } from "react";
import { Box, Input, Page, Swiper, Text } from "zmp-ui";
import CardShop from "../components/custom-card/card-shop";
import {
  activeCateState,
  activeFilterState,
  cartState,
  cartTotalPriceState,
  searchProductState,
  storeProductResultState,
  storeState,
} from "../state";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import CategoriesStore from "../components/categories-store";
import { filter } from "../constants/reference";
import { Product } from "../models";
import CardProductHorizontal from "../components/custom-card/card-product-horizontal";
import ButtonPriceFixed from "../components/button-fixed/button-price-fixed";
import { useNavigate } from "react-router-dom";
import ButtonFixed from "../components/button-fixed/button-fixed";
import HomeSwiper from "../components/custom-card/home-swiper";
import Categories from "../components/custom-card/category";
import RecommenedProduct from "../components/custom-card/recommend-product";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { selectStore } from "../redux/store/selectors";
import { getStoreRequest } from "../redux/store/actions";

const HomePage: React.FunctionComponent = () => {
  const [activeCate, setActiveCate] = useRecoilState<number>(activeCateState);
  const [activeFilter, setActiveFilter] =
    useRecoilState<string>(activeFilterState);
  const totalPrice = useRecoilValue<number>(cartTotalPriceState);
  const storeProductResult = useRecoilValue<Product[]>(storeProductResultState);
  const cart = useRecoilValue(cartState);
  const navigate = useNavigate();
  const setSearchProduct = useSetRecoilState(searchProductState);
  const dispatch = useAppDispatch();
  const store = useAppSelector(selectStore);
  useEffect(() => {
    // Dispatch action để yêu cầu tạo dữ liệu cửa hàng giả khi component được mount
    dispatch(getStoreRequest());
  }, []);
  console.log(store.store.categories);
  const hanleInputSearch = useCallback((text: string) => {
    setSearchProduct(text);
  }, []);
  const searchBar = useMemo(
    () => (
      <Input.Search
        placeholder="Tìm kiếm sản phẩm"
        onSearch={hanleInputSearch}
        className="cus-input-search"
      />
    ),
    []
  );
  return (
    <Page className="page" style={{ backgroundColor: "white" }}>
      {store && storeProductResult && (
        <>
          <div className="bg-primary">
            <CardShop storeInfo={store.store} />
            <Input.Search
              placeholder="Tìm kiếm sản phẩm"
              onSearch={hanleInputSearch}
              className="cus-input-search"
            />
            <HomeSwiper />
            <Suspense>
              <Text className="mb-5" style={{ fontWeight: "bold" }}>
                {" "}
                Tiện ích{" "}
              </Text>
              <Categories />
            </Suspense>
            {/* <Suspense>
              <Text className="mb-5" style={{ fontWeight: "bold" }}>
                Mặt hàng bán chạy{" "}
              </Text>
              <RecommenedProduct />
            </Suspense> */}
            <CategoriesStore
              categories={store.store.categories}
              activeCate={activeCate}
              activeFilter={activeFilter}
              setActiveCate={setActiveCate}
              setActiveFilter={setActiveFilter}
              filter={filter}
              quantity={storeProductResult.length}
            />
          </div>
          <div className="bg-gray-200 h-3" />
          <div
            className="bg-white p-3"
            style={{ marginBottom: totalPrice > 0 ? "120px" : "0px" }}
          >
            {storeProductResult.map((product) => (
              <div className="mb-2 w-full" key={product.id}>
                <CardProductHorizontal
                  pathImg={product.imgProduct}
                  nameProduct={product.nameProduct}
                  salePrice={product.salePrice}
                  retailPrice={product.retailPrice}
                  productId={product.id}
                />
              </div>
            ))}
          </div>
          {/* {totalPrice > 0 && (
            <>
              <ButtonPriceFixed
                quantity={cart.listOrder.length}
                totalPrice={totalPrice}
                handleOnClick={() => {
                  navigate("/finish-order");
                }}
              />
              <ButtonFixed
                listBtn={[
                  {
                    id: 1,
                    content: "Hoàn tất đơn hàng",
                    type: "primary",
                    onClick: () => {
                      navigate("/finish-order");
                    },
                  },
                ]}
                zIndex={99}
              />
            </>
          )} */}
        </>
      )}
    </Page>
  );
};

export default HomePage;
