const cx = (...args: any[]) => args.filter((args) => !!args).join("");

export default cx;
