import { useCallback } from "react";

import { useSetRecoilState } from "recoil";
import { initialProductInfoPickedState, productInfoPickedState } from "../../state";

const useResetProductPicked = () => {
  const setProductPicked = useSetRecoilState(productInfoPickedState);
  return useCallback(
    () => setProductPicked(initialProductInfoPickedState),
    [setProductPicked]
  );
};
export default useResetProductPicked;