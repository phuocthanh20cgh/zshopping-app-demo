import React, { Suspense, useEffect } from 'react';
import { Route } from 'react-router-dom'
import { App, ZMPRouter, AnimationRoutes, SnackbarProvider, Spinner } from 'zmp-ui';
import { RecoilRoot } from 'recoil';
import HomePage from '../pages';
import { Provider } from 'react-redux';
import { store, persistor } from '../redux/configStore';
import { ConfigProvider, getConfig } from './config.provider';
import { hexToRgb } from '../utils/hex-to-rgb';
import { PersistGate } from 'redux-persist/integration/react';
import DetailProduct from '../pages/detail-product';
import FinishOrder from '../pages/finish-order';
import Profile from '../pages/profile-user';
import BottomNavigationPage from './navigator';



const MyApp = () => {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ConfigProvider
          cssVariables={{
            "--zmp-primary-color": getConfig((c) => c.template.primaryColor),
            "--zmp-primary-color-rgb": hexToRgb(
              getConfig((c) => c.template.primaryColor)
            ),
          }}
        ><RecoilRoot>

            {/*giúp quản lý và xử lý các trạng thái chờ đợi trong quá trình rendering component*/}
            <Suspense
              fallback={
                <div className="w-screen h-screen flex justify-center items-center">
                  <Spinner />
                </div>
              }
            >
              <App >
                <SnackbarProvider>
                  <ZMPRouter>
                    <AnimationRoutes>
                      <Route path="/" element={<HomePage></HomePage>}></Route>
                      <Route path="/detail-product/:productId" element={<DetailProduct></DetailProduct>}></Route>
                      <Route path="/finish-order" element={<FinishOrder></FinishOrder>}></Route>
                      <Route path="/profile" element={<Profile></Profile>}></Route>

                    </AnimationRoutes>
                    <BottomNavigationPage size="large" />
                  </ZMPRouter>
                </SnackbarProvider>
              </App>
            </Suspense>

          </RecoilRoot>
        </ConfigProvider>
      </PersistGate>
    </Provider>

  );
};
export default MyApp;
