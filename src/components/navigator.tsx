import React, { useState } from "react";
import { BottomNavigation, Icon, Page } from "zmp-ui";
import { CiShoppingCart } from "react-icons/ci";
const BottomNavigationPage = (props) => {
    const [activeTab, setActiveTab] = useState("chat");
    const { title } = props;
    return (
        <Page className="page">
            <BottomNavigation
                fixed
                activeKey={activeTab}
                onChange={(key) => setActiveTab(key)}
            >
                <BottomNavigation.Item
                    key="chat"
                    label="Trang chủ"
                    icon={<Icon icon="zi-home" />}
                    activeIcon={<Icon icon="zi-home" />}
                    linkTo="/"
                />

                <BottomNavigation.Item
                    label="Giỏ hàng"
                    key="discovery"
                    icon={<CiShoppingCart />}
                    activeIcon={<CiShoppingCart />}
                    linkTo="/"
                />
                <BottomNavigation.Item
                    label="Thông báo"
                    key="contact"
                    icon={<Icon icon="zi-notif" />}
                    activeIcon={<Icon icon="zi-notif" />}
                    linkTo="/finish-order"

                />
                <BottomNavigation.Item
                    key="me"
                    label="Cá nhân"
                    icon={<Icon icon="zi-user" />}
                    activeIcon={<Icon icon="zi-user" />}
                    linkTo="/profile"

                />
            </BottomNavigation>
        </Page>
    );
};

export default BottomNavigationPage;
