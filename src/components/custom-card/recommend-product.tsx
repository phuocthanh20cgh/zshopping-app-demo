import React from 'react';
import { useRecoilValue } from 'recoil';
import { Box, Text } from 'zmp-ui';
import { recommendProductsState } from '../../state';
import convertPrice from '../../utils/convert-price';
import { Swiper, SwiperSlide } from "swiper/react";

const RecommendedProduct = () => {
    const recommendProducts = useRecoilValue(recommendProductsState);

    return (
        <Swiper slidesPerView={1.25} spaceBetween={16} className="flex flex-wrap" style={{ display: 'flex', flexDirection: 'row' }} direction="horizontal">
            {recommendProducts.map((product, index) => (
                <SwiperSlide key={index} style={{ flex: "50%", display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <Box
                        className="aspect-video rounded-lg bg-cover bg-center bg-skeleton"
                        style={{ marginRight: '16px', backgroundColor: "gray" }}
                    >
                        <img src={product.imgProduct} />
                        {product.retailPrice && (
                            <Text
                                size="xxxxSmall"
                                className="absolute right-2 top-2 uppercase bg-green text-white h-4 px-[6px] rounded-full"
                            >
                                Giảm {product.salePrice}
                            </Text>
                        )}
                    </Box>
                    <Box className="space-y-1">
                        <Text size="small">{product.nameProduct}</Text>
                        <Text size="xxSmall" className="line-through text-gray">
                            <span>đ</span>{convertPrice(product.retailPrice)}
                        </Text>
                        <Text size="large" className="font-medium text-primary">
                            <span>đ</span>{convertPrice(product.salePrice)}
                        </Text>
                    </Box>
                </SwiperSlide>
            ))
            }
        </Swiper >
    );
};

export default RecommendedProduct;
