import React from 'react'
import { useRecoilValue } from 'recoil'
import { Box, Swiper } from 'zmp-ui'

const HomeSwiper = () => {
    const imageSwiper = [
        { src: "https://stc-zmp.zadn.vn/zmp-zaui/images/0e05d63a7a93a6cdff826.jpg", alt: "slide-1" },
        { src: "https://stc-zmp.zadn.vn/zmp-zaui/images/0f7c061caab576eb2fa45.jpg", alt: "slide-2" },
        { src: "https://stc-zmp.zadn.vn/zmp-zaui/images/677fad2e0187ddd984969.jpg", alt: "slide-3" },
    ]
    return (
        <>
            <Box
                mt={4}
                flex
                flexDirection="column"
                justifyContent="center"
                alignItems="center"
                mb={5}
            >
                <Swiper autoplay={true} duration={6000}>
                    {imageSwiper.map((img, index) => {
                        return (
                            <Swiper.Slide >
                                <img
                                    key={index}
                                    className="slide-img"
                                    src={img.src}
                                    alt={img.alt}
                                />
                            </Swiper.Slide>
                        )
                    })}
                </Swiper>
            </Box>
        </>
    )
}

export default HomeSwiper