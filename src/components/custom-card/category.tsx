import React from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { Box, Text } from 'zmp-ui'
import { categoriesState, selectedCategoryIdState } from '../../state';
import { useNavigate } from 'react-router-dom';

const Categories = () => {
  const categories = useRecoilValue(categoriesState);
  const setSelectedCategoryId = useSetRecoilState(selectedCategoryIdState);
  const navigate = useNavigate();
  const gotoCategory = (categoryId: string) => {
    setSelectedCategoryId(categoryId);
    navigate("/category");
  };
  return (
    <Box mb={10} style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>

      {categories.map((category, i) => (
        <div
          key={i}
          onClick={() => gotoCategory(category.id)}
          className=""
          style={{ flex: "1 0 25%", display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}
        >
          <img className="w-12 h-12" src={category.icon} alt={category.name} />
          <Text size="xxSmall" className="text-gray">
            {category.name}
          </Text>
        </div>
      ))}
    </Box>




  )
}

export default Categories